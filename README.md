# Documentation


This is a multilingual [SolidShops](http://www.solidshops.com) theme that can be configured for one or more languages. 

Currently the following languages are supported but you can easily add more languages:

* dutch
* english
* french
* german
* spanish

This theme uses [bower](http://bower.io/) for frontend dependencies and has a [grunt](http://gruntjs.com/) task for automatically compiling your scss files. The default grunt task also includes a [livereload](http://livereload.com/) watch wich can automatically update your browser after each change you make trough your IDE.



## Theme Configuration
The theme contains a **config partial** where the following variables can be configured:


* **lang\_multilanguage** (boolean): you can set this variable to true if you want to enable multiple languages. If this variable is set to false, the default language of your store will be used.
* **lang\_multilanguage\_list** (array) : contains the list of languages you want to use if the lang\_multilanguage variable is set to true
* **lang\_customfieldgroup\_products** (string) : contains the name of the customfieldgroup for products
* **lang\_customfieldgroup\_pages** (string) : contains the name of the customfieldgroup for pages
* **social\_url\_x** (url) : the link for each social media logo
* **payment images** (array) : a list of all payment methods that are enabled in your store

## SolidShops Data

### Set your default language

First of all you need to define the "default" language of your store. This is the language that is used when the customer visits www.yourdomain.com/.
This language can be set [here](https://admin.solidshops.com/settings/general).



###Pages

Create a page custom field group **translations**. Add a custom field(dropdown) called **language** with the language you want:

* dutch
* english
* french
* german
* spanish

If you want to create an "about us" page for instance, you need to create 5 pages in the admin and select the correct language from this language dropdown for each page. 

###Products

Create a product custom field group **translations**.
Add the following fields to this customfieldgroup:

* nlname
* nldescription
* enname
* endescription
* frname
* frdescription
* dename
* dedescription
* esname
* esdescription

It's recommended to use a textfield for the name field and a wysiwyg field for the description field. 

###Categories

Categories can be created while creating/updating products.

If you enabled the lang_multilanguage variable in the config partial your category structure should look like this:

* nl
* -> Een categorie
* -> Een andere categorie
* en
* -> A category
* -> Another category    
* ...


 