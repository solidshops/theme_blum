module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css/sass',
                    src: ['**/*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        jshint: {
            theme: ['js/*.js.twig']
        },
        watch: {
            scss: {
                files: ['**/*.scss'],
                tasks: ['sass'],
                options: {
                    livereload: false,
                }
            },
            files: {
                files: ['**/*.js', '**/*.css', '**/*.twig', '**/*.html', '**/*.json'],
                tasks: [],
                options: {
                    livereload: true,
                }
            },
        },


    });


    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('js', ['jshint:theme']);
    grunt.registerTask('default', ['css','watch']);

};